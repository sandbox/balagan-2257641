<?php

/**
 * @file
 * Configurations form for the garmin communicator control module.
 */

/**
 * Administration settings for garmin communicator control
 */
function garmin_communicator_control_admin_settings_form() {
  $form = array();
  $form['garmin_communicator_control_gmap3'] = array(
    '#type' => 'textfield',
    '#title' => t('Key for google map v3'),
    '#default_value' => variable_get('garmin_communicator_control_gmap3'),
  );
  $form['garmin_communicator_control_garmin_plugin_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key for the Garmin communicator plugin.'),
    '#default_value' => variable_get('garmin_communicator_control_garmin_plugin_key'),
  );
  $form['garmin_communicator_control_garmin_url'] = array(
    '#type' => 'textfield',
    '#title' => t('The url that is associated with the Garmin key.'),
    '#default_value' => variable_get('garmin_communicator_control_garmin_url'),
  );
  $form['garmin_communicator_control_callback'] = array(
    '#type' => 'textfield',
    '#title' => t('Callback to process the data read from the GPS device.'),
    '#default_value' => variable_get('garmin_communicator_control_callback'),
  );
  return system_settings_form($form);
}