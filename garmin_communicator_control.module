<?php
/**
 * @file
 * garmin communicator control module file
 */

/**
 * Implements hook_menu().
 */
function garmin_communicator_control_menu() {
  $items['garmin_upload'] = array(
    'title' => 'Upload gpx file straight from a GPS device.',
    'description' => 'Upload a GPX file straight from a GPS device.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('garmin_communicator_control_garmin_upload_form'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/garmin_communicator_control'] = array(
    'title' => 'Garmin communicator control',
    'description' => 'Settings of the Garmin communicator control modul.',
    'weight' => -10,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('garmin_communicator_control_admin_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'garmin_communicator_control.admin.inc',
  );
  $items['admin/config/garmin_communicator_control/settings'] = array(
    'title' => 'Garmin communicator control settings',
    'description' => 'Change the settings of Garmin communicator control.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('garmin_communicator_control_admin_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'garmin_communicator_control.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_form().
 */
function garmin_communicator_control_garmin_upload_form($form, &$form_state) {
  $gmap_keyv3 = variable_get('garmin_communicator_control_gmap3');
  $garmin_key = variable_get('garmin_communicator_control_garmin_plugin_key');
  $garmin_url = variable_get('garmin_communicator_control_garmin_url');
  $gmap_url = "http://maps.google.com/maps/api/js?sensor=false";
  if (isset($gmap_keyv3) && isset($gmap_url)) {
    $gmap_url .= "&key=" . $gmap_keyv3;
  }
  $form['object'] = array(
    '#markup' => '<div style="height:0px; width:0px;"><object id="GarminNetscapePlugin" type="application/vnd-garmin.mygarmin" width="0" height="0">&nbsp;</object></div>',
  );
  $form['progress_bar'] = array(
    '#markup' => '<div id="statusBox"><div id="statusText">Plug-in initialized.  Find some devices to get started.</div>' .
      '<div id="progressBar" style="display: none;" align="left">' .
      '<div id="progressWrapper"><div id="progressBarDisplay">&nbsp;</div></div></div></div>',
  );
  $form['find_devices'] = array(
    '#prefix' => '<div id="deviceBox">',
    '#type' => 'button',
    '#value' => t('Find Devices'),
    '#attributes' => array(
      'id' => 'findDevicesButton',
    )
  );
  $form['cancel_find_devices'] = array(
    '#type' => 'button',
    '#value' => t('Cancel find devices'),
    '#attributes' => array(
      'id' => 'cancelFindDevicesButton',
      'disabled' => 'true',
    ),
  );
  $form['select_devices'] = array(
    '#type' => 'select',
    '#options' => array(
      '-1' => t('No devices found'),
    ),
    '#attributes' => array(
      'name' => 'deviceSelect',
      'id' => 'deviceSelect',
      'disabled' => 'true',
    ),
    '#suffix' => '</div>',
  );
  $form['filetype_select'] = array(
    '#type' => 'select',
    '#options' => array(
      'GPSData' => t('GPSData'),
      'FitnessHistory' => t('TCX History (all)'),
      'FitnessCourses' => t('TCX Courses (all'),
      'FitnessHistoryDirectory' => t('TCX History Directory'),
      'FitnessCoursesDirectory' => t('TCX Courses Directory'),
      'FitnessWorkouts' => t('TCX Workouts'),
      'FitnessUserProfile' => t('TCX User Profile'),
      'FitnessActivityGoals' => t('TCX Activity Goals'),
    ),
    '#attributes' => array(
      'name' => 'fileTypeSelect',
      'id' => 'fileTypeSelect',
      'disabled' => 'true',
    ),
    '#prefix' => '<div id="readBox">File type:', ///ITT kell valamit csinálnis vagy nem
  );
  $form['read_data'] = array(
    '#type' => 'button',
    '#value' => t('Read from device'),
    '#attributes' => array(
      'id' => 'readDataButton',
      'disabled' => 'true',
    )
  );
  $form['cancel_read'] = array(
    '#type' => 'button',
    '#value' => t('Cancel read'),
    '#attributes' => array(
      'id' => 'cancelReadDataButton',
      'disabled' => 'true',
    )
  );
  $form['activity_listing_header'] = array(
    '#markup' => '<br>Listing:<table id="activityListingHeader" cellpadding="3px"><tbody><tr><td>' .
    '<input type="checkbox" id="checkAllBox" disabled=""></td><td><b>Activity</b></td></tr>' .
    '</tbody></table><table id="activityListing" cellpadding="3px"><tbody>' .
    '<tr><td colspan="3">No activities listed.</td></tr></tbody></table>',
  );
  $form['read_selected'] = array(
    '#type' => 'button',
    '#value' => t('Read Selected from Device'),
    '#attributes' => array(
      'id' => 'readSelectedButton',
      'disabled' => 'true',
    )
  );

  $form['remove_all_activities'] = array(
    '#prefix' => '</div><div>',
    '#type' => 'button',
    '#value' => t('Remove all'),
    '#attributes' => array(
      'name' => 'removeAllActivitiesButton',
      'id' => 'removeAllActivitiesButton',
      'disabled' => 'true',
    )
  );
  $form['remove_selected_activities'] = array(
    '#type' => 'button',
    '#value' => t('Remove'),
    '#attributes' => array(
      'name' => 'removeActivityButton',
      'id' => 'removeActivityButton',
      'disabled' => 'true',
    ),
  );

  $form['selected_activities'] = array(
    '#type' => 'select',
    '#options' => array(
      '-1' => t('No activities selected'),
    ),
    '#attributes' => array(
      'name' => 'selectedActivitiesSelect',
      'id' => 'selectedActivitiesSelect',
      'size' => 10,
      //'disabled' => 'true',
    ),
    '#prefix' => '<br>Selected activities:<br>',
  );
  $form['upload_activities'] = array(
    '#prefix' => '<br>',
    '#suffix' => '</div>',
    '#type' => 'button',
    '#value' => t('Upload'),
    '#attributes' => array(
      'name' => 'uploadActivitiesButton',
      'id' => 'uploadActivitiesButton',
    ),
  );


  $form['select_routes'] = array(
    '#type' => 'select',
    '#options' => array(
      '-1' => t('No routes found'),
    ),
    '#attributes' => array(
      'name' => 'readRoutesSelect',
      'id' => 'readRoutesSelect',
      'disabled' => 'true',
    ),
    '#prefix' => '<div><br>Routes:',
  );
  $form['add_route'] = array(
    '#prefix' => '<br>',
    '#type' => 'button',
    '#value' => t('Add route'),
    '#attributes' => array(
      'name' => 'addRouteButton',
      'id' => 'addRouteButton',
      'disabled' => 'true',
    ),
  );
  $form['add_all_routes'] = array(
    '#type' => 'button',
    '#value' => t('Add all routes'),
    '#attributes' => array(
      'name' => 'addAllRoutesButton',
      'id' => 'addAllRoutesButton',
      'disabled' => 'true',
    ),
  );
  $form['select_tracks'] = array(
    '#type' => 'select',
    '#options' => array(
      '-1' => t('No tracks found'),
    ),
    '#attributes' => array(
      'name' => 'readTracksSelect',
      'id' => 'readTracksSelect',
      'disabled' => 'true',
    ),
    '#prefix' => '<br>Tracks:',
  );
  $form['add_track'] = array(
    '#prefix' => '<br>',
    '#type' => 'button',
    '#value' => t('Add track'),
    '#attributes' => array(
      'name' => 'addTrackButton',
      'id' => 'addTrackButton',
      'onclick' => 'return (false);',
      'disabled' => 'true',
    ),
  );
  $form['add_all_tracks'] = array(
    '#type' => 'button',
    '#value' => t('Add all tracks'),
    '#attributes' => array(
      'name' => 'addAllTracksButton',
      'id' => 'addAllTracksButton',
      'disabled' => 'true',
    ),
  );
  $form['select_waypoints'] = array(
    '#type' => 'select',
    '#options' => array(
      '-1' => t('No waypoints found'),
    ),
    '#attributes' => array(
      'name' => 'readWaypointsSelect',
      'id' => 'readWaypointsSelect',
      'disabled' => 'true',
    ),
    '#prefix' => '<br>Waypoints:',
  );
  $form['add_waypoints'] = array(
    '#prefix' => '<br>',
    '#type' => 'button',
    '#value' => t('Add waypoint'),
    '#attributes' => array(
      'name' => 'addWaypointButton',
      'id' => 'addWaypointButton',
      'disabled' => 'true',
    ),
  );
  $form['add_all_waypoints'] = array(
    '#type' => 'button',
    '#value' => t('Add all waypoints'),
    '#attributes' => array(
      'name' => 'addAllWaypointsButton',
      'id' => 'addAllWaypointsButton',
      'disabled' => 'true',
    ),
  );

  $form['map'] = array(
    '#markup' => '<div id="readMap" style="width: 100%; height: 400px; position: relative; background-color: rgb(229, 227, 223); overflow: hidden; -webkit-transform: translateZ(0);"><div></div><div></div><div class="gm-style" style="position: absolute; left: 0px; top: 0px; ' .
    'overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; ' .
    'overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; ' .
    'z-index: 1; width: 100%; cursor: url(http://maps.gstatic.com/mapfiles/openhand_8_8.cur) 8 8, default; -webkit-transform-origin: 0px 0px; ' .
    '-webkit-transform: matrix(1, 0, 0, 1, 0, 0);"><div style="-webkit-transform: translateZ(0); position: absolute;' .
    'left: 0px; top: 0px; width: 100%; z-index: 200;"><div style="-webkit-transform: translateZ(0); position: absolute; ' .
    'left: 0px; top: 0px; z-index: 101; width: 100%;"></div></div><div style="-webkit-transform: translateZ(0); position: absolute; ' .
    'left: 0px; top: 0px; width: 100%; z-index: 201;"><div style="-webkit-transform: translateZ(0); position: absolute; left: 0px; ' .
    'top: 0px; z-index: 102; width: 100%;"></div><div style="-webkit-transform: translateZ(0); position: absolute; left: 0px; top: 0px; ' .
    'z-index: 103; width: 100%;"></div></div><div style="-webkit-transform: translateZ(0); position: absolute; left: 0px; top: 0px; ' .
    'width: 100%; z-index: 202;"><div style="-webkit-transform: translateZ(0); position: absolute; left: 0px; top: 0px; z-index: 104; ' .
    'width: 100%;"></div><div style="-webkit-transform: translateZ(0); position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;">' .
    '</div><div style="-webkit-transform: translateZ(0); position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;">' .
    '</div></div><div style="-webkit-transform: translateZ(0); position: absolute; left: 0px; top: 0px; z-index: 100; '.
    'width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; ' .
    'left: 0px; top: 0px; z-index: 1;"></div></div></div><div style="position: absolute; z-index: 0;">' .
    '<div style="overflow: hidden;"></div></div></div></div></div></div></div>',
  );
  $form['hidden_gpx_data'] = array(
    '#type' => 'hidden',
    '#attributes' => array(
      'id' => 'hiddenGpxData',
    ),
  );
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/prototype.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/Util-Broadcaster.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/Util-BrowserDetect.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/Util-DateTimeFormat.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/Util-PluginDetect.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/Util-XmlConverter.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminMeasurement.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminSample.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminSeries.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminActivity.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GpxActivityFactory.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/TcxActivityFactory.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminObjectGenerator.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminObjectGenerator.js';
  $form['#attached']['js'][$gmap_url] =  array('type' => 'external');
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/load.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminGpsDataStructures.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminPluginUtils.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminDevice.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GoogleMapController.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminDevicePlugin.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminDeviceControl.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'garmin_communicator_control') . '/js/GarminDeviceControlDemo.js';
  $basepath = drupal_get_path('module', 'garmin_communicator_control');
  drupal_add_js(array('garmin_communicator_path' => $basepath), 'setting');
  if (isset($garmin_key) && isset($garmin_url)) {
    drupal_add_js(array('garmin_communicator_key' => $garmin_key), 'setting');
    drupal_add_js(array('garmin_communicator_url' => $garmin_url), 'setting');
  }
  //dpm($form, 'form');
  return $form;
}
/**
 * Submit callback of garmin_communicator_control_garmin_upload_form.
 */
function garmin_communicator_control_garmin_upload_form_submit($form, &$form_state) {
  $garmin_communicator_control_callback = variable_get('garmin_communicator_control_callback');
  $garmin_communicator_control_callback($form, &$form_state);
}
