(function ($) {
  Drupal.behaviors.garmin_controller = {
    attach: function (context, settings) {
	  var display;
	  //console.log(settings);
	  if ((typeof settings.garmin_key != 'undefined') && (typeof settings.garmin_url != 'undefined')) {
	  	my_url = settings.garmin_url;
	  	garmin_key = settings.garmin_key;
	  }
	  else {
	  	my_url = "http://developer.garmin.com/";
		garmin_key = "ee3934433a35ee348583236c2eeadbc1";
	  }
	  display = new GarminDeviceControlDemo(
		"statusText", 
		"readMap",
		[my_url, garmin_key]
	  );	
    }
  }
}(jQuery));